﻿using System;

namespace M17_UF1_E1_MyVector_CarboAlvaro
{
    class Program
    {
        static void Main(string[] args)
        {
            bool run = true;
            while (run)
            {
                Console.WriteLine("------------------------------------------\nWelcome to the menu, what would you like to do?\n1.Say Hello\n2.Write a vector\n3.Generate random vectors array\n4.Exit");
                int opt = Convert.ToInt32(Console.ReadLine());
                switch (opt)
                {
                    case 1:
                        Hello();
                        break;
                    case 2:
                        Vector();
                        break;
                    case 3:
                        Console.WriteLine("Write the array lenght");
                        int lenght = Convert.ToInt32(Console.ReadLine());
                        MyVector[] array = VectorGame.randomVectors(lenght);
                        bool runSort = true;
                        while (runSort)
                        {
                            Console.WriteLine("Do you want to sort the vector by distance(1) or by distance to origin(2), exit(3).");
                            int sortOption = Convert.ToInt32(Console.ReadLine());
                            switch (sortOption)
                            {
                                case 1:
                                    VectorGame.SortVectors(array, true);
                                    break;
                                case 2:
                                    VectorGame.SortVectors(array, false);
                                    break;
                                case 3:
                                    runSort = false;
                                    break;
                                default:
                                    Console.WriteLine("Wrong option, choose other.");
                                    break;
                            }
                        }
                        break;
                    case 4:
                        run = false;
                        break;
                    default:
                        Console.WriteLine("Wrong option, choose other.");
                        break;
                }
            }
        }

        static void Hello()
        {
            String name;
            Console.WriteLine("Write your name: ");
            name = Console.ReadLine();
            Console.WriteLine("Hello {0}", name);
        }

        static void Vector()
        {
            Console.WriteLine("Write a 2D (x,y) coordenate: ");
            int x1 = Convert.ToInt32(Console.ReadLine());
            int y1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write a second 2D (x,y) coordenate: ");
            int x2 = Convert.ToInt32(Console.ReadLine());
            int y2 = Convert.ToInt32(Console.ReadLine());

            MyVector myVector = new MyVector(x1, x2, y1, y2);

            myVector.CalculateVector();
            myVector.ChangeDirection();

            Console.WriteLine("\nMyVector:\n" + myVector);
        }


    }

    class MyVector
    {
        public double x1 { get; set; }
        public double x2 { get; set; }
        public double y1 { get; set; }
        public double y2 { get; set; }
        public double xy { get; set; }
        public double xyneg;

        public MyVector() { }

        public MyVector(double x1, double x2, double y1, double y2)
        {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
        }

        public void CalculateVector()
        {
            xy = Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        //Ha revisar amb l'enunciat
        public void ChangeDirection()
        {
            xyneg = -xy;
        }

        public override string ToString()
        {
            return "Coord A: (" + x1 + ", " + y1 + ")\n"
                + "Coord B: (" + x2 + ", " + y2 + ")\n"
                + "Vector distance: " + xy + "\n"
                + "Vector distance, changed direction: " + xyneg + "\n";
        }
    }
    class VectorGame
    {
        public static MyVector[] randomVectors(int lenght)
        {
            Console.WriteLine("Random vector array: ");
            Random random = new Random();
            MyVector[] arrayVector = new MyVector[lenght];
            MyVector vector;

            for (int i = 0; i < lenght; i++)
            {
                vector = new MyVector(random.Next(-99, 99), random.Next(-99, 99), random.Next(-99, 99), random.Next(-99, 99));
                arrayVector[i] = vector;
                vector.CalculateVector();
                vector.ChangeDirection();
            }
            // Mostra per pantalla
            foreach (MyVector i in arrayVector)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("------------------------------------------");

            return arrayVector;
        }

        public static MyVector[] SortVectors(MyVector[] x, bool opt)
        {
            MyVector[] SortedArray = x;
            if (opt == true)
            {
                MyVector temp;
                for (int i = 0; i < SortedArray.Length - 1; i++)
                {
                    for (int j = 0; j < SortedArray.Length - 1; j++)
                    {
                        if (x[j].xy < x[j + 1].xy)
                        {
                            temp = SortedArray[j + 1];
                            SortedArray[j + 1] = SortedArray[j];
                            SortedArray[j] = temp;
                        }
                    }
                }
                // Mostra per pantalla
                Console.WriteLine("\nSort by distance largest to smallest: ");
                foreach (MyVector i in SortedArray)
                {
                    Console.WriteLine(i);
                }
                Console.WriteLine("------------------------------------------");
            }
            else
            {
                double[] origin = new double[x.Length];
                MyVector temp;
                double dista;
                double distb;
                for (int i = 0; i < origin.Length; i++)
                {
                    dista = Math.Sqrt(Math.Pow((x[i].x1), 2) + Math.Pow((x[i].y1), 2));
                    distb = Math.Sqrt(Math.Pow((x[i].x2), 2) + Math.Pow((x[i].y2), 2));
                    if (dista < distb)
                    {
                        origin[i] = dista;
                    }
                    else
                    {
                        origin[i] = distb;
                    }
                }
                for (int i = 0; i < SortedArray.Length + 1; i++)
                {
                    for (int j = i + 1; j < SortedArray.Length; j++)
                    {
                        if (origin[i] > origin[j])
                        {
                            temp = SortedArray[i];
                            SortedArray[i] = SortedArray[j];
                            SortedArray[j] = temp;
                        }
                    }
                }
                // Mostra per pantalla
                Console.WriteLine("\nSort by distance to origin from smallest to largest: ");
                foreach (MyVector i in SortedArray)
                {
                    Console.WriteLine(i);
                }
                Console.WriteLine("------------------------------------------");
            }
            return SortedArray;
        }
    }
}